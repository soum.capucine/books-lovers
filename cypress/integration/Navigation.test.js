describe("Navigation", () => {
  it("Navigate between pages as intended", () => {
    cy.visit("/");
    cy.get("a").first().click();
    cy.url().should(
      "be.equal",
      cy.config().baseUrl + "/872179f2-4de2-4cde-a259-ee470d83d515"
    );
    cy.get("button").click();
  
    cy.url().then((result) =>
      expect(result).to.be.oneOf([cy.config().baseUrl, cy.config().baseUrl + "/"])
    );
  });
});
