import i18next from "../../src/i18n";

describe("page that loads data", () => {
  it("should show the loading message when loading the data then hide it afterwards", () => {
    // Create a Promise and capture a reference to its resolve
    // function so that we can resolve it when we want to
    let sendResponse;
    const trigger = new Promise((resolve) => {
      sendResponse = resolve;
    });

    // Intercept requests to the URL we are loading data from and do not
    // let the response occur until our above Promise is resolved
    cy.intercept(
      "https://raw.githubusercontent.com/sing2gather/impala-exercise/main/data.json",
      (request) => {
        return trigger.then(() => {
          request.reply();
        });
      }
    );

    // Now visit the page and assert the loading message is shown
    cy.visit("/");

    cy.contains(`${i18next.t("emptyTable")}`).should("be.visible");
    cy.contains(`${i18next.t("loading")}`)
      .should("be.visible")
      .then(() => {
        // After we've successfully asserted the loading message is
        // visible, call the resolve function of the above Promise
        // to allow the response to the data request to occur
        sendResponse();
        // ...and assert the mesage is removed from the DOM and
        // the data is shown instead.
        cy.contains(`${i18next.t("loading")}`).should("not.exist");
        cy.contains(`${i18next.t("emptyTable")}`).should("not.exist");
        cy.get("td").its("length").should("be.greaterThan", 1); // if there is data, should have on td per book
      });
  });
});
