import React from "react";
import { findBooksByAuthor, removeBook } from "./utilityFunctions";

describe("utilityFunctions", () => {
  describe("'findBooksByAuthor' function", () => {
    test("returns 2 matches", () => {
      const author = "Mrs. John Doe";
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
      ];

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("returns 0 match", () => {
      const author = "Zola";
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [];

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("'author' is undefined", () => {
      const author = undefined;
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [];

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("'books' is an empty array", () => {
      const author = "Zola";
      const books = [];

      const expectedResult = [];

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("'author' property doesn't exist", () => {
      const author = "Zola";
      const books = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];

      const expectedResult = [];

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("'books' is undefined", () => {
      const author = "Zola";
      const books = undefined;

      const expectedResult = undefined;

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });

    test("'books' and 'author' are undefined", () => {
      const author = undefined;
      const books = undefined;

      const expectedResult = undefined;

      expect(findBooksByAuthor(author, books)).toEqual(expectedResult);
    });
  });

  describe("removeBook function", () => {
    test("1 element removed", () => {
      const bookId = 3;
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 4, author: "Test" },
      ];

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("0 element removed", () => {
      const bookId = 5;
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("'bookId' is undefined", () => {
      const bookId = undefined;
      const books = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      const expectedResult = [
        { id: 1, author: "Mrs. John Doe" },
        { id: 2, author: "Mrs. John Doe" },
        { id: 3, author: "Mrs. Jonny Doe" },
        { id: 4, author: "Test" },
      ];

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("'books' is undefined", () => {
      const bookId = 5;
      const books = undefined;

      const expectedResult = undefined;

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("'books' and 'bookId' are undefined", () => {
      const bookId = undefined;
      const books = undefined;

      const expectedResult = undefined;

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("'books' is an empty array", () => {
      const bookId = 3;
      const books = [];

      const expectedResult = [];

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });

    test("'id' property doesn't exist", () => {
      const bookId = 3;
      const books = [
        { author: "Mrs. John Doe" },
        { author: "Mrs. John Doe" },
        { author: "Mrs. Jonny Doe" },
        { author: "Test" },
      ];

      const expectedResult = [
        { author: "Mrs. John Doe" },
        { author: "Mrs. John Doe" },
        { author: "Mrs. Jonny Doe" },
        { author: "Test" },
      ];

      expect(removeBook(books, bookId)).toEqual(expectedResult);
    });
  });
});
