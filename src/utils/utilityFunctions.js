/**
 * Return a list of books written by a specific author
 * @param {string} author - name of the author
 * @param {Object[]} books - list of books
 * @returns {type}
 */
export const findBooksByAuthor = (author, books) => {
  return books?.filter((el) => el.author === author);
};

/**
 * Remove a book from the list of books
 * @param {Object[]} books - list of books
 * @param {string} bookId
 * @returns {type}
 */
export const removeBook = (books, bookId) => {
  return books?.filter((el) => el.id !== bookId);
};
