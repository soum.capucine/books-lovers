import React from "react";
import ReactDOM from "react-dom";
import App from "./app/App";
import { store } from "./app/store";
import { Provider } from "react-redux";

import { BrowserRouter } from "react-router-dom";

// provide the global style and the style theme
/** @jsxImportSource @emotion/react */
import { Global, ThemeProvider } from "@emotion/react";
import { GlobalStyles } from "./indexStyle";
import { theme } from "./theme";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Global styles={GlobalStyles} />
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
