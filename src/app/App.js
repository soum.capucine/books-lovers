import React from "react";
import { HeaderStyle, MainStyle } from "./AppStyle.js";
import { Routes, Route, Navigate } from "react-router-dom";
import i18next from "../i18n";
import { Books } from "../features/books/Books.js";
import { BookDetails } from "../common/BooksDetails/BookDetails";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setBooks } from "../features/books/booksSlice";
import { url } from "../common/constants";

function App() {
  const dispatch = useDispatch();

  // Fetch books and save them to the store when the component is first rendered
  useEffect(() => {
    dispatch(setBooks(url));
  });

  return (
    <>
      <HeaderStyle>
        <h1>{i18next.t("appTitle")}</h1>
      </HeaderStyle>
      <MainStyle>
        <Routes>
          <Route path="/" element={<Books />} />
          <Route path="/:bookId" element={<BookDetails />} />
          /> />
          {/* Match only when no other routes do */}
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </MainStyle>
    </>
  );
}

export default App;
