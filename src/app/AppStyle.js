import styled from "@emotion/styled";
import { breakpoint500 } from "../common/constants";

export const HeaderStyle = styled.header`
  display: flex;
  justify-content: center;
  margin: 1rem;
`;

export const MainStyle = styled.main`
  display: flex;
  flex-direction: column;
  margin: 1rem 2rem;
  @media screen and (max-width: ${breakpoint500}) {
    margin: 1rem;
  }
`;
