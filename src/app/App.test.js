import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";
import { createMemoryHistory } from "history";
import App from "./App";
import { Router } from "react-router-dom";

test("redirect to home page when pathname='/872179f2-4de2/test'", () => {
  const history = createMemoryHistory();
  history.push("/872179f2-4de2/test");
  render(
    <Provider store={store}>
      <Router location={history.location} navigator={history}>
        <App />
      </Router>
    </Provider>
  );
  expect(window.location.pathname).toBe("/");
});
