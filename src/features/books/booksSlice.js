import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchBooks } from "../../API/fetchBooks";

const initialState = {
  books: [],
  loading: "idle",
};

export const setBooks = createAsyncThunk("books/setBooks", async (url) => {
  const responseJSON = await fetchBooks(url);
  return responseJSON.books;
});

const booksSlice = createSlice({
  name: "books",
  initialState,
  extraReducers: {
    [setBooks.pending]: (state, action) => {
      state.loading = "loading";
    },
    [setBooks.fulfilled]: (state, action) => {
      state.loading = "fulfilled";
      // Always fill state.books with a cleaned state
      state.books = [];
      action.payload.map((el) => state.books.push(el));
    },
    [setBooks.rejected]: (state, action) => {
      state.loading = "rejected";
    },
  },
});

export default booksSlice.reducer;

// Define a selector function
export const selectBooks = (state) => state.books.books;
export const selectLoading = (state) => state.books.loading;
