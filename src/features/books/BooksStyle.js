import styled from "@emotion/styled";
import { DataTable } from "primereact/datatable";
import { breakpoint620 } from "../../common/constants";

// Edit the PrimeReact theme
export const DataTableStyle = styled(DataTable)`
  font-family: "Montserrat", sans-serif !important; // need to use important to override PrimeReact theme

  .p-datatable,
  .p-datatable-tbody > tr {
    background: ${(props) => props.theme.colors.whiteOpacity4};
  }

  .p-datatable,
  .p-sortable-column:not(.p-highlight):not(.p-sortable-disabled):hover {
    background-color: ${(props) => props.theme.colors.whiteOpacity10};
  }

  .p-datatable,
  .p-datatable-tbody > tr > td > .p-column-title {
    margin-right: 2rem;
  }

  .p-datatable,
  .p-paginator-bottom,
  .p-datatable-thead > tr > th {
    background-color: ${(props) => props.theme.colors.whiteOpacity4};
  }

  .p-datatable[pr_id_1],
  .p-datatable-tbody > tr > td {
    @media screen and (max-width: ${breakpoint620}) {
      text-align: right;
    }
  }

  .book-cover {
    width: 150px;
    @media screen and (max-width: ${breakpoint620}) {
      width: 100px;
    }
  }
`;
