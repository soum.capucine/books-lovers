import { useDispatch, useSelector } from "react-redux";
import { selectBooks, selectLoading, setBooks } from "./booksSlice";
import { breakpoint620 } from "../../common/constants";
import { Link } from "react-router-dom";
import { url } from "../../common/constants";
import i18next from "../../i18n";

import { Column } from "primereact/column";
import { Button } from "primereact/button";
import "primeicons/primeicons.css"; //icons
import "primereact/resources/themes/md-light-indigo/theme.css";
import "primereact/resources/primereact.min.css"; //core css
import { DataTableStyle } from "./BooksStyle";

import { ToastContainer } from "react-toastify";

export const Books = () => {
  // select data from the store
  const books = useSelector(selectBooks);
  const loadingState = useSelector(selectLoading);

  const dispatch = useDispatch();

  // Define the refresh button
  const paginatorLeft = (
    <Button
      type="button"
      icon="pi pi-refresh"
      className="p-button-text"
      onClick={() => {
        dispatch(setBooks(url));
      }}
    />
  );

  // Define the content of the 'Cover' column
  const imageBodyTemplate = (rowData) => {
    return (
      <img
        src={`${rowData.cover}`}
        onError={(e) =>
          (e.target.src =
            "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
        }
        alt="Book cover"
        className="book-cover"
      />
    );
  };

  return (
    <>
      {loadingState === "loading" && i18next.t("loading")}
      
      <DataTableStyle
        value={books}
        responsiveLayout="stack"
        breakpoint={breakpoint620}
        removableSort
        paginator
        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink RowsPerPageDropdown"
        currentPageReportTemplate={`{first} - {last} ${i18next.t("element", {
          count: 2,
        })} ${i18next.t("of")} {totalRecords}`}
        rows={3}
        rowsPerPageOptions={[3, 5, 10]}
        paginatorLeft={paginatorLeft}
        emptyMessage={i18next.t("emptyTable")}
      >
        <Column field="title" header={i18next.t("title")} sortable></Column>
        <Column field="author" header={i18next.t("author")} sortable></Column>
        <Column
          field="cover"
          header={i18next.t("cover")}
          body={imageBodyTemplate}
          sortable
        ></Column>
        <Column
          header={i18next.t("link")}
          body={(rowData) => (
            <Link to={`/${rowData.id}`}>{i18next.t("information")}</Link>
          )}
        ></Column>
      </DataTableStyle>
      <ToastContainer position="top-left" />
    </>
  );
};
