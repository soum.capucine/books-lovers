import { useSelector } from "react-redux";
import { selectBooks } from "../../features/books/booksSlice";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { ArticleStyle, ButtonStyle } from "./BookDetailsStyle";
import i18next from "../../i18n";
import { findBooksByAuthor, removeBook } from "../../utils/utilityFunctions";

export const BookDetails = () => {

   // select data from the store
  const books = useSelector(selectBooks);

  // Find the selected book id
  const pathname = useLocation().pathname.split("");
  pathname.splice(0, 1);
  const bookId = pathname.join("");
  const book = books.find((el) => el.id === bookId) || {}; // defensive strategy to avoid bugs if book is falsy

  // Find the books written by the author
  const booksByAuthor = findBooksByAuthor(book.author, books);

  let navigate = useNavigate();

  return (
    <>
      <ArticleStyle>
        <h2>{book.title}</h2>
        <div className="container">
          <img src={book.cover} alt="Book cover" />
          <div className="book-information">
            <h3>{book.author}</h3>
            <p>{`ISBN: ${book.isbn}`}</p>
            <div className="other-books">
              <h3>{i18next.t("authorBooks")}</h3>
              <ul>
                {booksByAuthor.length > 1 // more books written than the one already displayed
                  ? removeBook(booksByAuthor, book.id).map((el) => (
                      <li key={el.id}>
                        <Link to={`/${el.id}`}>{el.title}</Link>
                      </li>
                    ))
                  : i18next.t("emptyList")}
              </ul>
            </div>
          </div>
        </div>
      </ArticleStyle>
      <ButtonStyle
        label={i18next.t("home")}
        onClick={() => {
          navigate("/");
          window.scroll(0, 0);
        }}
      />
    </>
  );
};
