import styled from "@emotion/styled";
import {
  breakpoint900,
  breakpoint620,
  breakpoint500,
} from "../constants";
import { Button } from "primereact/button";

export const ArticleStyle = styled.article`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 3rem;
  
  @media screen and (max-width: ${breakpoint500}) {
    margin-bottom: 1rem;
  }
  h2 {
    @media screen and (max-width: ${breakpoint500}) {
      text-align: center;
    }
  }

  .container {
    display: flex;

    @media screen and (max-width: ${breakpoint500}) {
      flex-direction: column;
      align-items: center;
    }
    & img {
      @media screen and (max-width: ${breakpoint900}) {
        width: 400px;
      }
      @media screen and (max-width: ${breakpoint620}) {
        max-width: 300px;
      }
    }

    & .book-information {
      margin: 0 1rem;

      @media screen and (max-width: ${breakpoint500}) {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      & .other-books {
        margin: 2rem 0;

        @media screen and (max-width: ${breakpoint500}) {
          text-align: center;
          margin: 1rem 0;
          padding: 0 3rem;
        }
      }
    }
  }
`;

export const ButtonStyle = styled(Button)`
  align-self: flex-end;
  @media screen and (max-width: ${breakpoint500}) {
    align-self: center;
  }
`;
