export const theme = {
  colors: {
    whiteOpacity4: "hsla(0 , 0% , 0%, 4%)",
    whiteOpacity10: "hsla(0, 0% , 0%, 10%)",
  },
};
