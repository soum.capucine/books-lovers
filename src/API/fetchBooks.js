import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import i18next from "../i18n";

const axios = require("axios").default;

export const fetchBooks = async (url) => {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    if (error.response) {
      toast.error(i18next.t("errorLoading"));
      throw Error(
        `${error.response.data} ${error.response.status} ${error.response.headers}`
      );
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser
    } else if (error.request) {
      toast.error(i18next.t("errorLoading"));
      throw Error(error.request);

      // Something happened in setting up the request that triggered an Error
    } else {
      toast.error(i18next.t("errorLoading"));
      throw Error(`Error ${error.message}`);
    }
  }
};
