import { css } from "@emotion/react";

export const GlobalStyles = (theme) => css`
  @import url("https://fonts.googleapis.com/css2?family=Montserrat&display=swap");

  * {
    font-family: "Montserrat", sans-serif;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  h1,
  h2,
  h3 {
    margin: 1rem 0;
  }

  a {
    color: currentColor;
  }

  li {
    margin: 0 1rem;
  }
`;
