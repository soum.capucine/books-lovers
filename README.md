# Project Name

Books lovers is an application to see information about books.

Live demo [_here_](https://books-lovers.netlify.app/).

## Table of Contents

- [Start the app](#start-app)
- [Test the app](#start-app)
- [Main Technologies Used](#main-technologies-used)
- [Development Rules](#development-rules)
- [Roadmap](#roadmap)
- [Contact](#contact)

## Start the app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

- Run `npm install` to install the libraries
- Run `npm start` to run the app in development mode.\
- Open [http://localhost:3000](http://localhost:3000) to view the application in your browser.

## Test the app

- For the unit tests: run `npm test` to launches the test runner in the interactive watch mode.\
- For the integration test
  - run `npx cypress open` to open the Cypress Test Runner
  - choose a test to run
  - it will open a browser and display the test on live

## Main Technologies Used

- React - version 17.0.2
- Redux - version 4.2.0
- Emotion - version 11.9.0 : library for writing css styles with JavaScript (css-in-js). I used the package @emotion/react that is recommended for React. I also used @emotion/styled to create components that have styles attached to them
- PrimeReact UI framework
- React Router library to navigate to pages
- For this technical test, I wanted to discover Cypress. That's why I created 2 integration tests with it. There are not intented to be complete but rather to demonstrate its usefulness and its easiness to start with.

## Development Rules

Follow a lightweight version of GitFlow:

- Create feature branches and develop there
- Do regular commits
- When a feature is finished and tested, merge in develop
- The main branch will be used to deploy to production -> to publish it to the web

## Roadmap

- adapt the table width with the available space. It will prevent the vertical scroll needed with small screen size
- Improve test coverage

## Contact

Created by [Capucine Soum](mailto:soum.capucine@gmail.com) - feel free to contact me!
